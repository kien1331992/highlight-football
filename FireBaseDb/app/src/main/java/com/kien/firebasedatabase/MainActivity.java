package com.kien.firebasedatabase;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends YouTubeBaseActivity implements VideoArrayAdapter.onItemClick {
    public static final String YoutubeDeveloperKey = "AIzaSyC9aN2Ea3LwSiaLBtyehPEEiSTW573J2pg";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    @BindView(R.id.rvVideo)
    RecyclerView rvVideo;


    VideoArrayAdapter adapter;
    List<Video> arrVideo;
    private long maxVideo = 0;

    //    private YouTubePlayer YPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        arrVideo = new ArrayList<>();
        adapter = new VideoArrayAdapter(this, arrVideo, this);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvVideo.setLayoutManager(manager);
        rvVideo.setAdapter(adapter);

        DatabaseReference countVideo = FirebaseDatabase.getInstance().getReference("maxVideo");

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                maxVideo = (long) dataSnapshot.getValue();

                DatabaseReference database = FirebaseDatabase.getInstance().getReference("videos");

                ValueEventListener valueEventListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (int i = 0; i < (int) maxVideo; i++) {
                            DataSnapshot dsVideo = dataSnapshot.child("video_" + (i + 1));
                            Video video = dsVideo.getValue(Video.class);
                            arrVideo.add(video);
                        }
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                };
                database.addListenerForSingleValueEvent(valueEventListener);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        countVideo.addListenerForSingleValueEvent(valueEventListener);


    }

    @Override
    public void setEven(int position) {
        Intent intent = new Intent(MainActivity.this, ViewVideoActivity.class);
        intent.putExtra("id", arrVideo.get(position).getUrl());
        startActivity(intent);
    }
}
