package com.kien.firebasedatabase;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class VideoArrayAdapter extends RecyclerView.Adapter<
        VideoArrayAdapter.DetailsViewHolder> {
    List<Video> lvVideos;
    Context context;
    LayoutInflater inflater;
    public onItemClick even;

    public VideoArrayAdapter(Context ct, List<Video> _lvVideos, onItemClick _even) {
        this.context = ct;
        this.lvVideos = _lvVideos;
        inflater = LayoutInflater.from(this.context);
        this.even = _even;
    }

    @Override
    public DetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_video, parent, false);
        return new DetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DetailsViewHolder holder, int position) {
        final Video video = lvVideos.get(position);
        Glide.with(this.context).load("http://img.youtube.com/vi/" + video.getUrl() + "/0.jpg").into(holder.imvVideo);
        holder.titleVideo.setText(video.getTitle());
        holder.tvDate.setText(video.getDate());

    }

    @Override
    public int getItemCount() {
        return lvVideos.size();
    }


    public class DetailsViewHolder extends RecyclerView.ViewHolder {
        ImageView imvVideo;
        TextView titleVideo;
        TextView tvDate;

        public DetailsViewHolder(View itemView) {
            super(itemView);
            imvVideo = itemView.findViewById(R.id.imvVideo);
            titleVideo = itemView.findViewById(R.id.titleVideo);
            tvDate = itemView.findViewById(R.id.tvDate);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    even.setEven(getAdapterPosition());
                }
            });
        }
    }

    public interface onItemClick {
        void setEven(int position);
    }
}
